# Generated by Django 4.0.3 on 2022-04-08 18:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('domains', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='whois',
            options={'verbose_name_plural': 'Whois'},
        ),
        migrations.AddField(
            model_name='domain',
            name='registrar_whois',
            field=models.CharField(blank=True, max_length=256, null=True),
        ),
        migrations.AlterField(
            model_name='domain',
            name='nameservers',
            field=models.JSONField(default=list),
        ),
        migrations.AlterField(
            model_name='whois',
            name='address',
            field=models.JSONField(blank=True, default=list, null=True),
        ),
        migrations.AlterField(
            model_name='whois',
            name='email',
            field=models.CharField(max_length=150),
        ),
        migrations.AlterField(
            model_name='whois',
            name='name',
            field=models.CharField(max_length=150),
        ),
        migrations.AlterField(
            model_name='whois',
            name='phone',
            field=models.JSONField(blank=True, default=list, null=True),
        ),
    ]
